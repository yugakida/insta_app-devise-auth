module HomeHelper
  def user_picture(user, size)
    if user.picture?
      image_tag user.picture.url, size: "#{size}", class: "rounded-circle"
    else
      random = [1, 2, 3, 4, 5]
      image_tag "default_image#{random.sample}.png", size: "#{size}", class: "rounded-circle"
    end
  end

  def current_user?(user)
    user == current_user
  end
end