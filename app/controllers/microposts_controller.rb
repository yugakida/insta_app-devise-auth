class MicropostsController < ApplicationController

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      redirect_to home_url(current_user), success: "投稿しました!"
    else
      redirect_to home_url(current_user), danger: "失敗しました…"
    end
  end

  def destroy
    Micropost.find(params[:id]).destroy
    redirect_to home_url(current_user), success: "消去しました"
  end

  private

    def micropost_params
      params.require(:micropost).permit(:content, :picture)
    end
end
