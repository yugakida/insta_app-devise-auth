require 'rails_helper'

RSpec.describe "home", type: :request do
  let!(:user){ create(:user) }

  describe "ログインしていないとリダイレクト" do
    it "GET /home" do
      get home_index_url 
      expect(response).to redirect_to(new_user_session_url)
    end

    it "GET /home/:id" do
      get home_url user.id
      expect(response).to redirect_to(new_user_session_url)
    end

    it "GET home/:id/followers" do
      get followers_home_url user.id
      expect(response).to redirect_to(new_user_session_url)
    end

    it "GET home/:id/following" do
      get following_home_url user.id
      expect(response).to redirect_to(new_user_session_url)
    end
  end

  describe "GET #index" do
    before do
      sign_in user
      get home_index_url
    end

    it "リクエストが成功すること" do
      expect(response.status).to eq 200
    end

    it "ユーザー名が表示されていること" do
      expect(response.body).to include user.name
    end
  end

  describe "GET #show" do
    before do
      sign_in user
      get home_url user.id
    end

    it "リクエストが成功すること" do
      expect(response.status).to eq 200
    end

    it "ユーザー名が表示されていること" do
      expect(response.body).to include user.name
    end
  end
end