require 'rails_helper'

RSpec.describe "relationships", type: :request do

  let!(:celeb){ create(:user, name: "celeb") }
  let!(:normal){ create(:user, name: "normal") }

  it "ログインしてないとフォローできない" do
    post relationships_path
    expect(response).to redirect_to new_user_session_path
  end

  it "ログインしていないとフォロー解除できない" do
    sign_in normal
    normal.follow(celeb)
    sign_out normal
    delete relationship_path celeb.id
    expect(response).to redirect_to new_user_session_path
  end
end