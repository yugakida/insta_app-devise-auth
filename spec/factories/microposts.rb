FactoryBot.define do
  factory :micropost do

    content { "awesome content" }

    association :user, factory: :user
  end
end
