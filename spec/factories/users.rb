FactoryBot.define do
  factory :user do
    name                  { "yuga" }
    sequence(:email)      { |n| "sample#{n}@example.com" }
    password              { "hogehoge" }
    password_confirmation { "hogehoge" }
    provider              { "facebook" }
  end
end
