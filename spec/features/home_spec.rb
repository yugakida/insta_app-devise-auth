require 'rails_helper'
require 'support/login_support'

RSpec.configure do |c|
  c.include LoginSupport
end

RSpec.feature "Home", type: :feature do

  given!(:user){ create(:user) }
  given!(:micropost){ build(:micropost, user: user) }

  feature "ログイン機能" do
    scenario "ログイン成功" do
      sign_in_as(user)
      expect(current_path).to eq home_path(user.id)
    end

    scenario "ログインユーザー名が表示される" do
      sign_in_as(user)
      within ".jumbotron" do
        expect(page).to have_content user.name
      end

      within first(".nav-item") do
        expect(page).to have_content user.name
      end
    end
  end

  feature "投稿機能" do
    scenario "投稿ができるか" do
      sign_in_as(user)
      fill_in "Write your feeling", with: "hogehoge"
      click_button "投稿"
      expect(user.microposts.count).to eq 1
    end

    scenario "投稿件数が表示されるか" do
      create_list(:micropost, 5, user: user)
      sign_in_as(user)
      expect(page).to have_content "あなたの投稿は、5件です。"
    end
  end

  scenario "消去するボタンが正常に動作するか" do
    create_list(:micropost, 5, user: user)
    sign_in_as(user)
    first(".card-body").click_link "消去する"
    expect(page).to have_content "あなたの投稿は、4件です。"
  end
end
