require 'rails_helper'

RSpec.feature "Welcome", type: :feature do
  
  before do
    visit root_path
  end

  feature "クリックしたら正しいページに遷移するか" do
    scenario "Insta App" do
      click_link "Insta App" 
      expect(current_path).to eq root_path
    end

    scenario "会員登録" do 
      click_link "会員登録"
      expect(current_path).to eq new_user_registration_path
    end

    scenario "ログイン" do
      click_link "ログイン"
      expect(current_path).to eq new_user_session_path
    end
  end
end
