require 'rails_helper'
require 'support/login_support'

RSpec.configure do |c|
  c.include LoginSupport
end

RSpec.feature "Relationships", type: :feature do
  given!(:celeb){ create(:user, name: "celeb") }
  given!(:normal){ create(:user, name: "normal") }

  it "フォローカウンターがきちんと動くか" do
    sign_in_as normal
    normal.follow(celeb)
    expect(normal.following.count).to eq 1
    visit current_path
    expect(page).to have_content "1 フォロー"
    click_link "ログアウト"
  end

  it "フォロワーカウンターがきちんと動くか" do
    sign_in_as celeb
    normal.follow(celeb)
    expect(celeb.followers.count).to eq 1
    visit current_path
    expect(page).to have_content "1 フォロワー"

  end
  
  it "フォロー、フォロー解除ボタン" do
    sign_in_as normal
    click_link "All Users"
    click_link "celeb"
    click_button "フォロー"
    expect(page).to have_content "1 フォロワー"
    click_button "フォロー解除"
    expect(page).to have_content "0 フォロワー"
  end

  it "show_followページ(フォロー)" do
    sign_in_as normal
    normal.follow(celeb)
    visit current_path
    click_link "1 フォロー"
    expect(current_path).to eq following_home_path normal.id 
    expect(page).to have_content "normal", "フォロー"
    expect(page).to have_link "celeb"
    expect(page).to have_link "view my profile"
  end

  it "show_followページ(フォロワー)" do
    sign_in_as celeb
    normal.follow(celeb)
    visit current_path
    click_link "1 フォロワー"
    expect(current_path).to eq followers_home_path celeb.id 
    expect(page).to have_content "celeb", "フォロワー"
    expect(page).to have_link "normal"
    expect(page).to have_link "view my profile"
  end
end
