require 'rails_helper'

RSpec.describe Relationship, type: :model do
  let!(:celeb){ create(:user, name: "celeb") }
  let!(:normal){ create(:user, name: "normal") }

  before do
    @relationship = Relationship.new(follower_id: normal.id, followed_id: celeb.id)
  end

  it "@relationshipが有効" do
    expect(@relationship).to be_valid
  end

  it "follower_idが必要" do
    @relationship.follower_id = nil
    expect(@relationship).to_not be_valid 
  end

  it "followed_idが必要" do
    @relationship.followed_id = nil
    expect(@relationship).to_not be_valid
  end

  it "follow, unfollow, following?メソッドが正しく動くか" do
    expect(normal.following?(celeb)).to be_falsey
    normal.follow(celeb)
    expect(normal.following?(celeb)).to be_truthy
    expect(celeb.followers).to eq [normal]
    normal.unfollow(celeb)
    expect(normal.following?(celeb)).to_not be_truthy
  end
end
