require 'rails_helper'

RSpec.describe User, type: :model do
  let!(:user){ create(:user) }

  it "has a valid factory" do
    expect(user).to be_valid
  end

  it "is invalid without a name" do
    invalid_user = build(:user, name: nil)
    expect(invalid_user).to be_invalid
  end
end
