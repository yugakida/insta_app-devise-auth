require 'rails_helper'

RSpec.describe Micropost, type: :model do

  it "has a valid factory" do
    micropost = create(:micropost)
    expect(micropost).to be_valid
  end

  describe "validation" do
    it "user_idなしでは投稿出来ない" do
      invalid_micropost = build(:micropost, user_id: nil)
      expect(invalid_micropost).to be_invalid
    end

    it "投稿文が存在しない" do
      blank_micropost = build(:micropost, content: " ")
      expect(blank_micropost).to be_invalid
    end

    it "投稿数が141文字以上" do
      too_long_micropost = build(:micropost, content: "a" * 141)
      expect(too_long_micropost).to be_invalid
    end
  end

  it "投稿リストが降順で表示される" do
    user = create(:user)
    older_micropost = create(:micropost, user: user, created_at: 1.month.ago)
    newer_micropost = create(:micropost, user: user, created_at: 1.day.ago)
    expect(user.microposts.to_a).to eq [newer_micropost, older_micropost]
  end
end
