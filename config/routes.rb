Rails.application.routes.draw do
  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks'
  }


  root 'welcome#index'
  resources :microposts, only:[:create, :destroy]
  resources :home do
    member do
      get :following, :followers
    end
  end
  resources :relationships, only:[:create, :destroy]
end
